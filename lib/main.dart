// подключение библиотеки с элементами
import 'package:flutter/material.dart';

// void main() {
//   runApp(MyApp());
// }

//более коротки вариант написания
void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  // Этот виджет является корнем приложения
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        // это "Тема" приложения
        // можно сменить цвет приложения поменяв значение Colors.blue, например
        // на Colors.green
        primarySwatch: Colors.blue,
        // visualDensity позволяет адаптировать приложение под определенную
        // платформу
        visualDensity: VisualDensity.adaptivePlatformDensity,
      ),
      // заголовок в шапке приложения
      home: MyHomePage(title: 'Демонстрационная страница Flutter'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);

  // Это виджет домашней страницы. Он имеет класс StatefulWidget,
  // а значит может менять внешний вид в зависимости от своего состояния

  // Этот класс конфигурирует состояние. Он содержит значение "заголовок"
  // предоставленный родителем (корнем приложения MyApp).
  // Поля виджета подкласса маркируются как "финальные" (final).

  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  int _counter = 0;

  void _incrementCounter() {
    setState(() {
      // Вызов setState сообщает Flutter об изменении состояния, что
      // перезапускает сборку и обновляет значения.
      // В противном случае при нажатии на кнопку результат был бы не виден.
      _counter++;
    });
  }

  @override
  Widget build(BuildContext context) {
    // Этот метод вызывается каждый раз, когда вызывается setState
    // This method is rerun every time setState is called, for instance as
    // done by the _incrementCounter method above.
    //
    // The Flutter framework has been optimized to make rerunning build
    // methods fast, so that you can just rebuild anything that needs
    // updating rather than having to individually change instances of widgets.
    return Scaffold(
      appBar: AppBar(
        // Here we take the value from the MyHomePage object that was created
        // by the App.build method, and use it to set our appbar title.
        title: Text(widget.title),
      ),
      body: Center(
        // Center is a layout widget. It takes a single child and positions it
        // in the middle of the parent.
        child: Column(
          // Column is also a layout widget. It takes a list of children and
          // arranges them vertically. By default, it sizes itself to fit its
          // children horizontally, and tries to be as tall as its parent.
          //
          // Invoke "debug painting" (press "p" in the console, choose the
          // "Toggle Debug Paint" action from the Flutter Inspector in Android
          // Studio, or the "Toggle Debug Paint" command in Visual Studio Code)
          // to see the wireframe for each widget.
          //
          // Column has various properties to control how it sizes itself and
          // how it positions its children. Here we use mainAxisAlignment to
          // center the children vertically; the main axis here is the vertical
          // axis because Columns are vertical (the cross axis would be
          // horizontal).
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Text(
              'You have pushed the button this many times:',
            ),
            Text(
              '$_counter',
              style: Theme.of(context).textTheme.headline4,
            ),
          ],
        ),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: _incrementCounter,
        tooltip: 'Increment',
        child: Icon(Icons.add),
      ),
      // This trailing comma makes auto-formatting nicer for build methods.
    );
  }
}
